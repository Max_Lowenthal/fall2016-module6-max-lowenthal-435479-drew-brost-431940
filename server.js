// Require the packages we will use:
var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs");
 
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	// This callback runs when a new connection is made to our HTTP server.
 
	fs.readFile("client.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.
 
		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
});
app.listen(3456);
 //main data structure
 var rooms1 = {
	MainRoom: {
	 
                        name: "MainRoom",
                        pass: "",
						owner: "No One",
						//list of whoever has ever been in it, and if they are currently
						memberList: {fakeUser: false},
						//password protected?
						passProt: false,
						//list of all past users, and banned or not
						bannedList: {},
						//list of all past users and whether they are in timeout (unused. Also, I know this is gross and should have been a single user list, potentially radically shifted from what we did - but this is rapid prototype development, not perfect code prototype development
						timeOutList: {},
						//who's afk
						afkList: {}
 }
 };
 
 var users = {};
 
// Do the Socket.IO magic:
var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
	// This callback runs when a new Socket.IO connection is established.
	
	//recieve messages
	socket.on('message_to_server', function(data) {
		// This callback runs when the server receives a new message from the client.
 
		console.log("message: "+data["message"] + "; room: " + data["room"]); // log it to the Node.JS output
		io.sockets.emit("message_to_client",{message:data["message"], room:data["room"], user:data.user, users:users}) // broadcast the message to other users
	});	
	
	//create a room
	socket.on('create_room',function(data){
				//var roomNum = Object.keys(rooms1).length - 1;
                var key = data.name;
				
				console.log("roomName: "+data["name"]);
				//console.log("roomName by room2: "+data["room2"]);
				
				console.log("room_pass: "+data["pass"]);
				//console.log("room_pass by roompass: "+data["room_pass"]);
				
				var userNombre = data.username;
				var protect = ""!==data["pass"];
                rooms1[key] = {name: data.name, pass: data.pass, owner:userNombre, memberList:{},bannedList:{}, timeOutList: {}, afkList:{}, passProt:protect};
				console.log("username: " + data.username);
				
				// rooms1[key].memberList.push(data.username);
                rooms1[key].memberList[data.username] = true;
				rooms1[key].bannedList[data.username] = false;
				rooms1[key].timeOutList[data.username] = false;
				rooms1[key].afkList[data.username] = false;
				
				rooms1[data.oldRoom].memberList[data.username] = false;
				
				for (x in rooms1[key].memberList) {
					console.log("member: " + x + " is " + rooms1[key].memberList[x]);
				}
				
                io.sockets.emit("update_list", {rooms: rooms1, users:users});
		
	});
	
	//check to see if original room name
	socket.on('newRoom',function(data){
		var included = false;
		for (x in rooms1) {
			if (x === data.newRoom) included = true;
		}
		var allowed = !included;
		io.sockets.emit("createNewRoom", {user: data.user, newRoom: data.newRoom, allowed:allowed});
	});
	
	//join room
	socket.on('joining_room_nopass',function(data){

		var output = "members are ";
		for (x in rooms1) {
			output += x + ", ";
		}

		rooms1[data.oldRoom].memberList[data.username] = false;
		
		rooms1[data.newRoom].memberList[data.username] = true;
		rooms1[data.newRoom].bannedList[data.username] = false;
		rooms1[data.newRoom].afkList[data.username] = false;
		
		var key = data.newRoom;
				for (x in rooms1[key].memberList) {
					console.log("member of room " + rooms1[key].name + ": " + x + " is " + rooms1[key].memberList[x]);
				}
		
		io.sockets.emit("update_list", {rooms: rooms1, users:users});
	});
	
	//run to update details
	socket.on('requestUpdate',function(data){
		io.sockets.emit("update_list", {rooms: rooms1, users:users});
	
	});
	
	//add someone to a room (without taking them out of previous room - used for init)
	socket.on('addToRoom',function(data){
		rooms1[data.newRoom].memberList[data.username] = true;
		rooms1[data.newRoom].bannedList[data.username] = false;
		rooms1[data.newRoom].afkList[data.username] = false;
		
		users[data.username] = {fontSet:false};
		
		console.log("adding " + data.username + " to  " + data.newRoom);
		io.sockets.emit("update_list", {rooms: rooms1, users:users});
	
	});
	
	//remove someone from a room; also does not put them anywhere. used for booting
	socket.on('removeFromRoom', function(data) {
		rooms1[data.oldRoom].memberList[data.username] = false;
		io.sockets.emit("update_list", {rooms: rooms1, users:users});
	});
	
	//verifies passwords for joining a room
	socket.on('passVerify', function(data) {
		var verify = rooms1[data.newRoom].pass == data.pass;
		io.sockets.emit("joinRoomPass", {ver:verify, room:data.newRoom, user:data.username});
	});
	
	//ban a user
	socket.on('banUser', function(data) { 
		rooms1[data.banRoom].bannedList[data.username] = true;
		console.log("trying to ban " +data.username + " from " + data.banRoom);
		var banName = data.username;
		io.sockets.emit("kickBan", {userBan:banName});
	});
	
	//kick a user
	socket.on('kickUser', function(data) { 
		console.log("trying to ban " +data.username + " from " + data.banRoom);
		var banName = data.username;
		io.sockets.emit("kickUser", {userBan:banName});
	});
	
	//is this user banned?
	socket.on('banCheck', function(data) { 
		var banned = rooms1[data.newRoom].bannedList[data.username];
		console.log("ban check on " +data.username + " from " + data.newRoom + " returned " + banned);
		io.sockets.emit("banFilter", {newRoom:data.newRoom, notOk:banned, user:data.username});
	});
	
	//send a private message
	socket.on('privateMessage', function(data) { 
		console.log("user " + data.username + " just tried to send " + data.recip + " " + data.msg);
		io.sockets.emit("privateMessageRecieved", {username:data.username, recip:data.recip, msg:data.msg, noReply:data.noReply});
	});
	
	//change user's server afk state
	socket.on('toggleAFK', function(data) {
		rooms1[data.room].afkList[data.username] = !rooms1[data.room].afkList[data.username];
		io.sockets.emit("update_list", {rooms: rooms1, users:users});
	});
	
	
	socket.on('fontUpdate', function(data) {
		console.log("setting font for " + data.username + " to " + data.fontStyle);
		users[data.username] = {fontSet:true, fontStyle:data.fontStyle};
		console.log("font is set: " + users[data.username].fontSet + "; font style is " + users[data.username].fontStyle);
		io.sockets.emit("update_list", {rooms: rooms1, users:users});
	});
	//defunct; for server timeout
	/*
	socket.on('ping', function(data) {
		io.sockets.emit("ping2", {user:data.user});
	});
	*/
});	


