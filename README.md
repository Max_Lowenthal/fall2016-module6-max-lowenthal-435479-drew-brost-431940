# **WARNING: This code and Module are optimized for Google Chrome. If you use a different browser, you're going to have a problem**

_EPISODE VI: RETURN OF THE ALL-NIGHTER_

 >LONG AGO IN AN URBAUER LAB FAR FAR AWAY  
 >TWO STUDENTS SPENT HOURS DESIGNING A CHATROOM SO PERFECTLY  
 >THAT IT WAS THE ENVY OF ALL OF THEIR CLASSMATES.  
 >THEY HID THE CODE IN A LOWLY R2-BITBUCKET-UNIT  
 >WAITING UNTIL THE RIGHT TA CAME ALONG TO MAKE USE OF IT  
 >AND RESTORE ORDER TO THE CLASSROOM.  
 >THAT TA IS YOU.  
 >HELP US 330TA, YOURE OUR ONLY HOPE.  
 
 (This is a Star Wars themed Write-up)
 
 
Actually though, It's Good to see you again. Thanks for taking the time to grade our Module 6. 

Names: Drew Brost and Max Lowenthal  
Student IDs: 431940 and 435479  
[Here's a link to our chatroom!](http://ec2-54-186-115-66.us-west-2.compute.amazonaws.com:3456)  
You might have some issues turning it on, but you're smart so I know you'll figure it out! :)

*Quick Explanation of our Private Message Feature*  
	 We wanted you to be able to slide into your friend's DMs with the greatest of ease, so we chose to approach the Private Message feature using Javascript's Window Alert feature, with an included text box in each for your responses (or your lack there of)
		However a bevy of window alerts every time someone wants to have a heart to heart is super annoying, and doesn't let you talk with any of your other peeps! For this reason, we've created an Inbox feature that stores all your window alert messages until 
		you're ready to look at them. The button won't appear unless you have a PM waiting for you, and when you do you simply click on it and it shows the message, along with a text box to respond if you so choose.

Part I: **The Phantom Keyboard** *(The ability to go AFK or Away From Keyboard for all you n00bz)*  
Description:  
	People complain that us Computer Science folk can never get off our laptops and go outside! Who can blame them though, with products as engaging as our chatroom you wouldn't wanna step away
	for even a second, you could miss an important message from a pal. Luckily, we've got you covered here, by creating a "Go AFK" button for our chatroom users, you can step away from your screen
	without worrying about missing a single message. This button has two main functions, the first helps you hide your chats from any pesky snoopers who may come around while you're away. It does this by hiding everything
	but the "Return from AFK" button once it is clicked, and returns the page to its previous state when clicked again. However, you don't wanna miss any important gossip while you're away. For this reason we've implemented an auto-response
	function for you when you're AFK. If you happen to be away and recieve a direct message, the message will be stored in your inbox for when you return, and the sender will automatically recieve a response letting them know that you aren't
	at your keyboard right now, and you'll answer when you get back! Going AFK also adds a "- AFK" next to your name in a User list in any chatroom. 
	
Part II:**Attack of the Noise** *(Instant Message Sound Implementation)*  
Description:  
	Honestly, who doesn't miss middle school?! (Max doesn't incase you were curious, Middle School sucked for him). However, if you're one of the lucky few who wants to jump back to the days of your old AIM
	screenname (Max's was likestotalk600, a true classic) then look no further! Everytime you send a message, you are blessed with the not-annoying-at-all classic AOL AIM noise on every machine connected to your chatroom. 
	This feature will be sure to let your friends know when they're missing out on some juicy gossip, and definitely won't bug you after the first 100 times you hear it (Author's Note: Can Confirm it does bug you after literally the second time).
	
Part III: **Revenge of the Idlers** *(Auto Sign Out after 5 Minutes)*  
Description:  
	Sometimes you just forget to go AFK, and leave your whole identity in the chatrooms at risk of being hijacked by any passerby! Just think, if some random ne'er–do–well strolled by and had access to YOUR
	private messages! What if your crush walked by and stumbled upon you talking about them!? We can't have that happening now can we? Our solution to this problem is to sign any user out after 5 minutes of inactivity. 
	To us, activity is defined as any user-generated function: sending a message, creating a chatroom etc. Also, if you happen to Go AFK you don't have to worry about getting signed out, we've paused the inactivity timer for you. 
	Returning from AFK will also reset your activity timer back to 0, so you can get right back to the action without any worry of being signed out. 
	
Part IV: **A New Font** *(You can change your Font)*  
Description:  
	We know that being stylish is important to the young-ones these days, and we're all about the aesthetic vibes here. To change up  your personal from your friends and the crowd, we've created the option to use 
	a few different fonts when viewing the chatroom The options: Times New Roman, Newspaper, Symbol, Papayrus (Undertale tho, great game) offer a variety of complex emotions, and allow you to express yourself in a way
	that the real world simply cant. By switching your font, the font of your text changes for everyone in a chatroom, so each person can be expressing themselves differently!
	
Part V: **The Broken Feature Strikes Back** *(This was scraped, just wanted some input on it)*  
Description:  
	So, we intially had the idea to offer users the ability to change their usernames whenever they wanted, and if my memory serves correctly the code for this button is still written up in our work. 
	Eventually, we decided to scrap the idea because we realized it could be used as a work-around for the Banning feature, where a user can simply just change their username and re-enter a room they were just permabanned from.
	This is an interesting moral dilemma we struggled with, do we trust the users to not to not do this? Or do we limit their creatvity and add a serious weight to their decision of username? Eventually we wanted to keep our rooms
	safe from the trolls, so we scrapped the idea. However, we'd love to hear your input on it, so feel free to Facebook message us or approach us and give us your thoughts.
	
_Conclusion_:  
	>AND SO THE DAY WAS SAVED ONCE AGAIN  
	>AS THE BRAVE TA MADE THEIR WAY THROUGH THE CODE  
	>UNDERSTANDING EACH LINE, AND REACHING OUT TO THE STUDENTS IF THEIR WERE ANY ISSUES (plz do this)
	>AND THE GALAXY OF 330 CONTINUED TO EXIST FOR ANOTHER DAY.
	
	Thanks. Have a good day/night, and remember the force is always with you. 

	